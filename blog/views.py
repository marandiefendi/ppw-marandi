from django.shortcuts import render

def home(request):
    return render(request, "web-ppw-css.html", {'title': 'home'})

def exped(request):
    return render(request, "bootstrap.html", {'title': 'exped'})

def skills(request):
    return render(request, "web-skills.html", {'title': 'skills'})

def contact(request):
    return render(request, "bscontact.html", {'title' : 'contact'})
