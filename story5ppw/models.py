from django.db import models
from datetime import datetime, date
from django.utils import timezone

# Create your models here.


class Schedule(models.Model):
    date = models.DateTimeField(default=timezone.now)
    time = models.TimeField()
    activity = models.CharField(max_length=30)
    place = models.CharField(max_length=20)
    category = models.CharField(max_length=20)