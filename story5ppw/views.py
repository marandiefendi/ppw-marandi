from django.shortcuts import render, redirect
from .models import Schedule
from . import forms
from django.http import HttpResponse

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
    if request.method == 'POST':
        id = request.POST['id']
        Schedule.objects.get(id=id).delete()
        return redirect('schedule')

